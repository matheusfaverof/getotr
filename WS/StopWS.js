/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
//@Descrição: Realiza o Logout da Conta do OTRS, e encerra o Navegador aberto.
//@Autor:     Matheus Favero



/** Importação dos Modulos **/ 
const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');

/** Importa o Navegador Padrão para o Método **/ 
page = require('../OTRS/BuscaChamadosPorCliente/Navegador');
browser = require('../OTRS/BuscaChamadosPorCliente/Navegador');



//**  IMPLEMENTAÇÃO  **//



// == Função que realiza o encerramento da Execução do WebService == //
async function Stop(){
    EscreveConsole("Parando a Execução do WS")
    $LogoutRealizado = await Logout();
    if($LogoutRealizado == true){
        EscreveConsole("Realizado Logout da conta do OTRS com Sucesso!")
        browser.close();
    }
    EscreveConsole("WS encerrado com Sucesso");
    return 'WS Encerrado';
}

// == Função que Realiza o Logout no OTRS == //
async function Logout(){
    await page.goto("https://suporte.chapeco-solucoes.com.br/otrs/index.pl?", { waitUntil: 'networkidle0' });
    const content = await page.content();                                               //Pega o HTML da Pagina
    const $html = cheerio.load(content);                                                //Carrega o HTML
    
    _LogoutLink = "https://suporte.chapeco-solucoes.com.br" + $html('#AppWrapper').find('#LogoutButton').attr('href');
    
    await page.goto(_LogoutLink, { waitUntil: 'networkidle0' });                        //Navega para a Pagina de Logon
    //page.waitForNavigation({ waitUntil: 'networkidle0' }),                                   //Aguarda o Carregamento da Pagina
    await page.close();
    await browser.close();
}

// == Função que realiza a exibição dos logs no console da VM == //
function EscreveConsole($Texto){
    console.log("[Atualizador] - " + new Date().toLocaleTimeString() + " - " + $Texto);
}

/** Modulo de Exportação dos Dados deste Arquivo **/
module.exports = {
    Stop: Stop,
};