/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
//@Descrição: Realiza a execução do WebService e controla a chamada dos Métodos, como suas validações
//@Autor:     Matheus Favero


//https://pt.linkedin.com/pulse/entendendo-par%C3%A2metros-em-requisi%C3%A7%C3%B5es-de-uma-vez-por-todas-henrique


// == Configurações do WebService REST == //

/// Importação do Modulo Express
const express = require('express');
/// Importação do Modulo Express
const app = express();
/// Porta do Servidor
const port = 8080;

// == Importações das Rotas do WS == //
const BuscaChamadoPorCliente = require('../OTRS/BuscaChamadosPorCliente/BuscaChamadoPorCliente');     //Busca os chamados abertos de um cliente especifico (Utilizado pelo Painel de Carteiras)
const BuscaChamadosSobGestao = require('../OTRS/BuscaChamadosSobGestao/BuscaChamadosSobGestao');      //Busca os Chamados sob gestão do Usuario
const BuscaChamadosProjetos  = require('../OTRS/BuscaChamadosProjetos/Main');                         //Busca os Chamados do Cliente que possui o tipo 'Projeto'
const StopWS = require('./StopWS');                                                                   //Chama a função para Fechar o Web Service



//**  IMPLEMENTAÇÃO  **//



// == Metodos == //

/** Realiza o processo de Encerramento do WebService */
app.get('/Stop', async (req, res, next) => {
    EscreveConsole("Requisição Recebida no Método /Stop");
    res.send(JSON.stringify(await StopWS.Stop()));
});

/** Realiza a Busca pelos Chamados Abertos do Cliente */
app.get('/BuscaChamadoPorCliente', async (req,res,next) => {
    let $Codigo = req.query.Codigo;  //Codigo do Cliente
    EscreveConsole("Requisição Recebida no Método /BuscaChamadoPorCliente utilizando o Cód do Cliente: " + $Codigo);
    res.send(JSON.stringify(await BuscaChamadoPorCliente.BuscaChamadoAbertoCliente($Codigo)))
});

/** Realiza a Busca por todos os Chamados associados ao Usuario */
app.get('/BuscaChamadosSobGestao', async (req,res,next) => {
    let $Codigo = req.query.Codigo;  //Codigo do Usuario
    EscreveConsole("Requisição Recebida no Método /BuscaChamadosSobGestao utilizando o Cód de Usuario: " + $Codigo);
    res.send(JSON.stringify(await BuscaChamadosSobGestao.BuscaChamadosSobGestao($Codigo)))
});

/** Realiza a Busca por todos os chamados do tipo Projeto do Cliente solicitado */
app.get('/BuscaProjetos', async (req,res,next) => {
   let $Codigo = req.query.Codigo; //Codigo do Cliente
    EscreveConsole('Requisição Recebida no Método /BuscaProjetos utilizando o Cod. do Cliente: ' + $Codigo);
    res.send(JSON.stringify(await BuscaChamadosProjetos.Main($Codigo)));
});


// == Metodo de Conexão ==//
app.listen(port, () => {
    EscreveConsole("Servidor Iniciado com Sucesso na porta " + port);
});

/** Função que realiza a insersão dos Logs na Tela do Console */
function EscreveConsole($Texto){
    console.log("[WebService] - " + new Date().toLocaleTimeString() + " - " + $Texto);
}