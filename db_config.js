/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
// @Descrição:  Classe de configuração da conexão com o Banco de Dados
// @Autor: Matheus Favero



// Função que Retorna um Pool de Conexão com o Banco de Dados
// Deve ser utilizado para todos os métodos de conexão do GetOTR

function Conexao(){

    /// Versão dos Arquivos
    const $GetOTRVersao = "rc.1.2.0";

    const { Pool } = require('./node_modules/pg');

    const PoolConexao =  new Pool({
        host: '31.220.60.220',              //Host  
        user: 'postgres',                   //Usuario
        password: 'Favero10@M',             //Senha
        database: 'GestorX',                //Banco de Dados
        port: 5432,                         //Porta
        max: 1000,                          //Quantidade maxima de Conexões do Pool
        idleTimeoutMillis: 30000,           //Tempo Maximo de Inatividade
        connectionTimeoutMillis: 0,         //Tempo do TimeOut
    });
    return PoolConexao;
}


// Metodo de Exportação
module.exports = {
    Conexao: Conexao,
};