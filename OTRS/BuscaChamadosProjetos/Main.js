/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
//@Descrição: Busca todos os Chamados do Cliente que possuem o tipo Projeto
//@Autor: Matheus Favero

/** Importação dos Modulos */
const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');
const {rows} = require("pg/lib/defaults");

/** Importa o Navegador Padrão para o Método **/
browser = require('../BuscaChamadosPorCliente/Navegador'); //Importa o Navegador

// Função que inicia a Execução do Processo e devolve os dados para a Requisição WS
// @Param $Codigo: Codigo do Cliente cadastrado no 'GestorX' (Recebido via WS)
// @Return Retorna o Status '' caso a execução ocorra com Sucesso, ou '' caso haja uma falha na execução

async function Main($Codigo){

    //Importa o Pool de Conexão com o Banco de Dados
    const $db_config = require('../../db_config');
    $PoolConexao = $db_config.Conexao();

    //EscreveConsole("Iniciando Processo de Atualização de Projetos do Cliente Cod: " + $Codigo, 'Log');
    try{
        $Execute = await BuscaProjetosOTRS($Codigo, $PoolConexao);
        if ($Execute == true){
            return 'Projetos Atualizados';
        }
        else if ($Execute == false){
            return 'Erro ao Atualizar os Projetos';
        }
    } catch (e) {
        return 'Erro na execução da Função';
    }
    //await GravaChamadosBanco("2022110750000751","GSAT","Limites de Velocidade Máxima baseado no Google Maps","Em Atendimento","Projetos","Matheus Favero","Matheus Favero","04/04/2023 12:40","04/04/2023 12:40",1, $PoolConexao);
}

// Função que Acessa e Executa o Scraping na Pagina do OTRS
// @Param $Codigo: Codigo do Cliente cadastrado no 'GestorX' (Recebido via WS)
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna 'True' caso a execução ocorra com sucesso | 'False' caso ocorra alguma exceção

async function BuscaProjetosOTRS($Codigo, $PoolConexao){

    const _ChamadosNumeroTicket = []; // Lista de Chamados Encontrados (Numero do Ticket)

    //Busca o Codigo do Cliente
    var $ClienteIDCustomer = await BuscaIDCliente($Codigo, $PoolConexao);

    if($ClienteIDCustomer == false){
        //Caso ocorrer um erro na Função, retorna False e encerra a execução da solicitação
        return false;
    }

    //Busca os Dados da Pagina
    var $OTRSProjetosPorCliente = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=AgentTicketSearch;Subaction=Search;StateType=Open;QueueIDs=9;CustomerID=' + $ClienteIDCustomer;
    try{
        const page = await browser.newPage();
        await page.goto($OTRSProjetosPorCliente,{ waitUntil: 'networkidle0' });
        const content = await page.content();
        var $html = await cheerio.load(content);
        page.close();
    } catch (e) {
        EscreveConsole("Ocorreu um erro ao obter os dados do OTRS: " + e.message, Error);
        return false;
    }

    // Variaveis de Controle da Execução
    var _LinhasQuantidade = $html('tr').length;
    var _FilaDeChamados = 0;

    //Processa os Dados encontrados
    for(var i = 0; i < _LinhasQuantidade; i++){
        let _Ticket =       $html('tr:eq('+ i +')').find('td:eq(3)').text().trim();                                                                     //Numero do Chamado
        let _ClienteNome =  $html('tr:eq('+ i +')').find('td:eq(4)').text().trim().slice(8);                                                            //Nome do Cliente
        let _Titulo =       $html('tr:eq('+ i +')').find('td:eq(5)').find('div').attr('title')?.replace("'", "");                                       //Titulo do Chamado
        let _Estado =       $html('tr:eq('+ i +')').find('td:eq(6)').find('div').attr('title');                                                         //Estado do Chamado
        let _Fila =         $html('tr:eq('+ i +')').find('td:eq(7)').find('div').attr('title');                                                         //Fila do Chamado
        let _Responsavel =  $html('tr:eq('+ i +')').find('td:eq(8)').find('div').attr('title');                                                         //Responsavel
        let _Proprietario = $html('tr:eq('+ i +')').find('td:eq(9)').find('div').attr('title');                                                         //Proprietario
        let _CriadoEm =     $html('tr:eq('+ i +')').find('td:eq(10)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Cadastro do Chamado
        let _AlteradoEm =   $html('tr:eq('+ i +')').find('td:eq(11)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Data da Alteração

        _ChamadosNumeroTicket.unshift("'"+_Ticket+"'"); //Adiciona o Numero do Chamado na Lista de Chamados Abertos
        _FilaDeChamados = _FilaDeChamados + await GravaChamadosBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm,$Codigo, $PoolConexao);
    }
    await AlteraStatusProjetos(_ChamadosNumeroTicket, $Codigo, $PoolConexao);
    if(_FilaDeChamados >= _LinhasQuantidade){
        return true;
    }
}

// Função que realiza a Inserção/Atualização/Edição dos dados do Chamado encontrado no Banco de dados
// @Param _ClienteID Codigo do Cliente cadastrado no 'GestorX' (Recebido via WS)
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @params Dados dos chamados encontrados

async function GravaChamadosBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm,_ClienteID, $PoolConexao){

    let _SQL_BuscaProjeto = "SELECT * FROM cprj_cadastro_projetos WHERE cprj_otrs_ticket = $1";
        let _SQL_BuscaProjetoValores = [_Ticket];
    let _SQL_InsereProjeto = "INSERT INTO public.cprj_cadastro_projetos (cprj_otrs_data_cadastro, cprj_otrs_titulo, cprj_otrs_status, cprj_otrs_ticket, cprj_otrs_responsavel, cprj_otrs_proprietario, cprj_otrs_data_alteracao, cprj_cli_codigo) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);";
        let _SQL_InsereProjetoValores = [_CriadoEm,_Titulo,_Estado,_Ticket, _Responsavel, _Proprietario,_AlteradoEm,_ClienteID];
    let _SQL_AtualizaProjeto = "UPDATE cprj_cadastro_projetos SET cprj_otrs_data_cadastro=$1, cprj_otrs_status=$2, cprj_otrs_ticket=$3, cprj_otrs_responsavel=$4, cprj_otrs_proprietario=$5, cprj_otrs_data_alteracao=$6, cprj_cli_codigo=$7 WHERE cprj_otrs_ticket = $8";
        let _SQL_AtualizaProjetoValores = [_CriadoEm,_Estado,_Ticket, _Responsavel, _Proprietario,_AlteradoEm,_ClienteID,_Ticket];
    let _SQL_InsereStatus = "INSERT INTO sprj_status_projetos (sprj_cprj_codigo, sprj_status, sprj_usua_codigo) VALUES ($1,$2,$3);";

    // Função que Verifica se este Projeto já está cadastrado
    // @Param $SQL_BuscaProjeto: SQL Padrão
    // @Param $SQL_BuscaProjetoValores: Valor do SQL
    // @Param $PoolConexao: Pool de Conexão
    // @Return Retorna 'True' se o Projeto existir

    async function VerificaCadastro($SQL_BuscaProjeto,$SQL_BuscaProjetoValores,$PoolConexao){
        try{
            return await new Promise((resolve, reject) => $PoolConexao.query($SQL_BuscaProjeto, $SQL_BuscaProjetoValores, (err, res) => {
                if (err) {
                    EscreveConsole("Ocorreu um Erro ao verificar o cadastro do Chamado!", Error);
                    EscreveConsole(err.message, Error);
                } else if (res.rowCount >= 1) {
                    resolve(true);
                } else {
                    resolve(false);
                }
                return resolve.valueOf();
            }));
        } catch (e) {
            EscreveConsole("Ocorreu um erro na execução da função VerificaCadastro(): " + e.message, Error);
            return false;
        }
    }

    // Função que Insere os dados do Projeto
    // @Param $SQL_InsereProjeto: SQL Padrão
    // @Param $SQL_InsereProjetoValores: Valor do SQL
    // @Param $SQL_InsereStatus: SQL Padrão para inserir o Status do Projeto
    // @Param $Ticket: Numero do Chamado no OTRS
    // @Param $PoolConexao: Pool de Conexão
    // @Return Retorna 'True' se o Projeto foi inserido com sucesso

    async function InsereProjeto($SQL_InsereProjeto,$SQL_InsereProjetoValores,$SQL_InsereStatus,$Ticket,$PoolConexao){
        try{
            //Insere o Projeto
            await $PoolConexao.query($SQL_InsereProjeto, $SQL_InsereProjetoValores, async (err, res) => {
                if (err) {
                    EscreveConsole("Ocorreu um erro ao inserir o Projeto!", Error);
                    EscreveConsole(err.message, Error);
                }

                //Insere o Status
                let $CodigoProjeto = await BuscaProjetoCodigo($Ticket, $PoolConexao);
                let _SQL_InsereStatusValores = [$CodigoProjeto, 'Novo', 0];
                $PoolConexao.query($SQL_InsereStatus, _SQL_InsereStatusValores, (err, res) => {
                    if (err) {
                        EscreveConsole("Ocorreu um erro ao inserir o Status do Projeto!", Error);
                        EscreveConsole(err.message, Error);
                    }
                });
            });
            return true;
        } catch (e) {
            EscreveConsole("Ocorreu um erro na execução da função InsereProjeto(): " + e.message, Error);
            return false;
        }
    }

    // Função que atualiza os dados do Projeto
    // @Param $SQL_AtualizaProjeto: SQL Padrão
    // @Param $SQL_AtualizaProjetoValores: Valor do SQL
    // @Param $PoolConexao: Pool de Conexão
    // @Return Retorna 'True' se o Projeto foi atualizado com sucesso

    async function AtualizaProjeto($SQL_AtualizaProjeto,$SQL_AtualizaProjetoValores,$PoolConexao){
        try{
            $PoolConexao.query($SQL_AtualizaProjeto, $SQL_AtualizaProjetoValores, (err,res) => {
                if(err){
                    EscreveConsole("Ocorreu um erro ao Atualizar o Projeto!", Error);
                    EscreveConsole(err.message, Error);
                }
            });
            return true;
        } catch (e) {
            EscreveConsole("Ocorreu um erro na execução da função AtualizaProjeto(): " + e.message, Error);
            return false;
        }
    }

    if(await VerificaCadastro(_SQL_BuscaProjeto,_SQL_BuscaProjetoValores, $PoolConexao) == true){
        AtualizaProjeto(_SQL_AtualizaProjeto, _SQL_AtualizaProjetoValores, $PoolConexao);
        return true;
    } else {
        InsereProjeto(_SQL_InsereProjeto, _SQL_InsereProjetoValores, _SQL_InsereStatus,_Ticket,$PoolConexao)
        return true;
    }
}

// Função que Busca o Codigo do Cliente do OTRS utilizando o Codigo do GestorX (De/Para)
// @Param $Codigo Codigo do Cliente no GestorX
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna o Codigo do Cliente do OTRS | Retorna 'False' caso ocorrer algum erro na execução

async function BuscaIDCliente($Codigo, $PoolConexao){
    var SQL = "SELECT cli_customer_id FROM cli_clientes cc where cli_codigo = " + $Codigo;
    return new Promise(async (resolve, reject) => await $PoolConexao.query(SQL, (err, res) => {
        if (err) {
            EscreveConsole("Ocorreu um Erro ao Buscar a Lista de Clientes: " + err.message, Error);
            return false;
        } else {
            var $Customer = res.rows[0]['cli_customer_id'];
            resolve($Customer);
        }
        return resolve;
    }));
}

// Função que realiza a alteração do Status dos Projetos no GestorX de acordo com os chamados do OTRS
// @Param $Chamados Array contendo o numero de todos os chamados encontrados na Pagina do OTRS
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Param $ClienteID Codigo do Cliente cadastrado no 'GestorX'

async function AlteraStatusProjetos($Chamados, $ClienteID, $PoolConexao){
    let _SQL_AlteraStatus = "UPDATE cprj_cadastro_projetos SET cprj_otrs_status = 'Encerrado' WHERE cprj_otrs_ticket NOT IN ($1) AND cprj_cli_codigo = $2;";
        let _SQL_AlteraStatusValores = [$Chamados,$ClienteID];

    if($Chamados.length > 0){
        try{
            $PoolConexao.query(_SQL_AlteraStatus, _SQL_AlteraStatusValores, (err,res) => {
                if(err){
                    EscreveConsole("Ocorreu um Erro ao alterar o status dos projetos Fechados!", 'Erro');
                    EscreveConsole("Mensagem de ERRO: " + err.message, 'Erro');
                }
            });
        } catch (e){
            EscreveConsole("Ocorreu um Erro na execução da função AlteraStatusProjetos: " + e.message, 'Erro');
        }
    }
}

// Função que Busca o Codigo do Projeto
// @Param $Ticket: Numero do Ticket do Projeto
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna o Codigo do Projeto

async function BuscaProjetoCodigo($Ticket, $PoolConexao){
    var SQL = "SELECT * FROM cprj_cadastro_projetos WHERE cprj_otrs_ticket = '" + $Ticket + "' LIMIT 1";
    return new Promise((resolve, reject) => $PoolConexao.query(SQL, (err, res) => {
        if (err) {
            EscreveConsole("Ocorreu um Erro ao Buscar o Projeto: " + err.message, Error);
            return false;
        } else if (res.rowCount >= 1) {
            var $Codigo = res.rows[0]['cprj_codigo'];
            resolve($Codigo);
            return resolve;
        } else {
            return false;
        }
    }));
}

// Função que escreve os logs no Console da VM
// @param $Texto Texto a ser exibido
// @param $Tipo Tipo da mensagem (Log, Erro)

function EscreveConsole($Texto, $Tipo){
    if($Tipo == 'Erro'){
        console.log("[Erro > BuscaChamadosProjetos] - " + new Date().toLocaleTimeString() + " - " + $Texto);
    } else {
        console.log("[BuscaChamadosProjetos] - " + new Date().toLocaleTimeString() + " - " + $Texto);
    }
}

/** Modulo de Exportação dos Dados */
module.exports = {
    Main: Main,
};