//Cria e Abre um Navegador ao iniciar o WebService
//Após realiza Login no OTRS usando as credenciais de acesso

//Importações
const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');

var _OTRSLoginURL =             'https://suporte.chapeco-solucoes.com.br/otrs/index.pl'     //Pagina de Login do OTRS
Main();

async function Main(){
    // == Configurações == //
    browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        headless: true,
        timeout: 15000000,
    });                                                           
    page = await browser.newPage();                             //Cria uma nova Pagina
    await page.setViewport({width: 1200, height: 720});         //Seta o ViewPort da Pagina
    await Login();                                              //Realiza Login no OTRS
    EscreveConsole("Navegador Aberto com Sucesso!");
    require('./BuscaUltimosChamadosCriados');
}

async function Login(){
    EscreveConsole("Realizando Login no OTRS");
    await page.goto(_OTRSLoginURL, { waitUntil: 'networkidle0' });                               // Navega para a Pagina de Login
    await page.type('#User', 'matheus.favero');                                                         // Usuario
    await page.type('#Password', 'Favero10@M');                                                         // Senha

    await Promise.all([
        page.click('#LoginButton'),                                    //Clica no Botão de Entrar
        page.waitForNavigation({ waitUntil: 'networkidle0' }),  //Aguarda o Carregamento da Pagina 
    ]);
    EscreveConsole("Login realizado com Sucesso, aguardando Instruções!");
    return true;
}

function EscreveConsole($Texto){
    console.log("[Atualizador] - " + new Date().toLocaleTimeString() + " - " + $Texto);
}