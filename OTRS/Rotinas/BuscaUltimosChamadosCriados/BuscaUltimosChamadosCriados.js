//Acessa a pagina de Chamados Criados
//Baixa todos os chamados criados nas ultimas horas, não considerando status
//Caso já existir um chamado criado, apenas atualiza as informações
//Deve ser rodado a cada 5 minutos

const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');

var _OTRSLoginURL =             'https://suporte.chapeco-solucoes.com.br/otrs/index.pl'        //Pagina de Login do OTRS
var _OTRSChamadosAbertosURL =   'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=AgentTicketSearch;Subaction=Search;TakeLastSearch=1;SaveProfile=1;Profile=GetOTR%20-%20UltimosCriados'
var _OTRSLogoutURL =            'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=Logout;ChallengeToken='

page = require('./Navegador');  //Importa o Navegador padrão para o método


//Teste de Gravação dos dados
//GravaChamadoBanco("2023033050000248","Apisul","RES: MOTORISTA SUSPENSO OPENTECH","Encerrado","Atendimento","Gabriel Spagnol","Gabriel Spagnol","30/03/2023 15:00","30/03/2023 15:00");

BuscaUltimosChamadosCriados();
async function BuscaUltimosChamadosCriados() {
    GravaLogBanco('BuscaUltimosChamadosCriados','Rotina Iniciada, aguardando tempo de Execução','Rotina');
    EscreveConsole("Rotina Iniciada, Preparando método de Busca");
    _BuscarChamados = await LoopingController();
    if(_BuscarChamados == true){
        EscreveConsole("A Rotina de Atualização foi Parada pelo Método LoopingController");
    }
}

async function LoopingController(){
    //Realiza o Controle do Looping Search dos Chamados
    EscreveConsole("Iniciando busca por novos chamados criados no OTRS");

    if (await BuscaConfiguracoes("BuscaUltimosChamadosCriados", "Rotina_Ativa") == "S"){
        setTimeout(function() {
            BuscaChamadosThread();      //Busca os Chamados
            LoopingController();        //Reinicia
        }, await BuscaConfiguracoes("BuscaUltimosChamadosCriados", "Tempo_Espera_Looping"));
    }
    else{
        EscreveConsole("A Rotina está desativada, o Status da Rotina será verificado novamente em 5 Minutos");
        setTimeout(function() {
            EscreveConsole("Tentanto iniciar a Rotina Novamente.");	
            LoopingController();
        }, 300000);
    }
}

async function BuscaChamadosThread(){
    EscreveConsole("Chamados Encontrados, realizando Gravação!");

    var _OTRSChamadosURL =   'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=AgentTicketSearch;Subaction=Search;TakeLastSearch=1;SaveProfile=1;Profile=GetOTR%20-%20UltimosCriados';
    await page.goto(_OTRSChamadosURL, { waitUntil: 'networkidle0' });               //Navega para a Pagina do Cliente
    const content = await page.content();                                           //Pega o HTML da Pagina
    const $html = cheerio.load(content);                                            //Carrega o HTML

    var _LinhasQuantidade = $html('tr').length;

    //Busca todos os chamados do cliente
    for(var i = 0; i < _LinhasQuantidade; i++){
        var _Ticket =       $html('tr:eq('+ i +')').find('td:eq(3)').text().trim();                                                                     //Numero do Chamado
        var _ClienteNome =  $html('tr:eq('+ i +')').find('td:eq(4)').text().trim().slice(8);                                                            //Nome do Cliente  
        var _Titulo =       $html('tr:eq('+ i +')').find('td:eq(5)').find('div').attr('title')?.replace("'", "");                                       //Titulo do Chamado
        var _Estado =       $html('tr:eq('+ i +')').find('td:eq(6)').find('div').attr('title');                                                         //Estado do Chamado
        var _Fila =         $html('tr:eq('+ i +')').find('td:eq(7)').find('div').attr('title');                                                         //Fila do Chamado
        var _Responsavel =  $html('tr:eq('+ i +')').find('td:eq(8)').find('div').attr('title');                                                         //Responsavel
        var _Proprietario = $html('tr:eq('+ i +')').find('td:eq(9)').find('div').attr('title');                                                         //Proprietario
        var _CriadoEm =     $html('tr:eq('+ i +')').find('td:eq(10)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Cadastro do Chamado
        var _AlteradoEm =   $html('tr:eq('+ i +')').find('td:eq(11)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Alteração do Chamado
        GravaChamadoBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm);
    }
    GravaLogBanco('BuscaUltimosChamadosCriados','Integrando ' + _LinhasQuantidade + 'Chamados','Rotina');
    EscreveConsole("Integrando " + _LinhasQuantidade + " Chamados com Sucesso!");
    return
}



async function GravaChamadoBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm){
    const { Pool } = require('../../node_modules/pg')                                //Importa o Postgres
    const pool = new Pool({host: '31.220.60.220',user: 'postgres',password: 'Favero10@M',database: 'GestorX',port: 5432});
    


    //Busca o Código do Cliente
    var SQL_BuscaClienteCod = "SELECT * FROM cli_clientes cc WHERE cli_nome_otrs ILIKE '%" + _ClienteNome +"%';";
    var _ClienteID = await new Promise((resolve, reject) => pool.query(SQL_BuscaClienteCod, (err, res) => {
        if(err){
            console.log(err.message);
            console.log(SQL_BuscaClienteCod);
        }

        if(res.rowCount >= 1){
            resolve(res.rows[0]['cli_codigo']);
        }
        else{
            resolve(null);
        }
        return resolve;
    }));

    var _SQL_BuscaChamado = "SELECT * FROM ccha_cliente_chamado ccc WHERE ccha_chamado_ticket = '" + _Ticket +"'";
    var _SQL_InsereChamado = "INSERT INTO public.ccha_cliente_chamado(ccha_chamado_ticket, ccha_chamado_titulo, ccha_otrs_estado, ccha_otrs_fila, ccha_otrs_data_cadastro, ccha_otrs_data_alteracao, ccha_otrs_responsavel, ccha_otrs_proprietario, ccha_cli_codigo) VALUES('"+ _Ticket + "', '"+_Titulo+"', '"+_Estado+"', '"+_Fila+"', '"+_CriadoEm+"', '"+_AlteradoEm+"','"+_Responsavel+"','"+_Proprietario+"', '"+_ClienteID+"');";
    var _SQL_AtualizaChamado = "UPDATE public.ccha_cliente_chamado SET ccha_chamado_titulo='"+_Titulo+"', ccha_otrs_estado='"+_Estado+"', ccha_otrs_fila='"+_Fila+"', ccha_otrs_data_cadastro='"+_CriadoEm+"', ccha_otrs_data_alteracao='"+_AlteradoEm+"', ccha_otrs_responsavel='"+_Responsavel+"', ccha_otrs_proprietario='"+_Proprietario+"' WHERE ccha_chamado_ticket = '"+_Ticket+"'";

    if(_ClienteID != null){
        //Verifica se já existe um Chamado Salvo com o Numero do Chamado recebido

        pool.query(_SQL_BuscaChamado, (err, res) => {
            if(err){
                console.log(err.message);
            }
            else if(res.rowCount == 0){
                pool.query(_SQL_InsereChamado, (err, res) => {
                    if(err){
                        console.log(err.message);
                        console.log(_SQL_InsereChamado);
                    }
                    EscreveConsole("Chamado " + _Ticket + " Inserido com Sucesso!");
                });
            }
            else {
                pool.query(_SQL_AtualizaChamado, (err, res) => {
                    if(err){
                        console.log(err.message);
                        console.log(_SQL_AtualizaChamado);
                    }
                    EscreveConsole("Chamado " + _Ticket + "Atualizado com Sucesso!");
                });
            }
        });
    }
    else{
        EscreveConsole(_Ticket + "Nome do Cliente '" + _ClienteNome + "' não encontrado no Banco de dados!");
    }
}





function EscreveConsole($Texto){
    console.log("[Rotina - UltimosChamados] - " + new Date().toLocaleTimeString() + " - " + $Texto);
}

async function GravaLogBanco($FuncaoNome, $FuncaoDescricao, $FuncaoTipo){
    const { Client } = require('../../node_modules/pg');
    const client =  new Client({host: '31.220.60.220',user: 'postgres',password: 'Favero10@M',database: 'GestorX',port: 5432});
    client.connect();
    client.query("INSERT INTO public.flog_funcoes_log (flog_codigo, flog_nome_funcao, flog_data_cadastro, flog_descricao, flog_tipo_funcao) VALUES(0, '"+ $FuncaoNome +"', now(), '"+ $FuncaoDescricao +"', '"+ $FuncaoTipo +"');", (err, res) => {
        if (err) throw err
        client.end();
    });
}

async function BuscaConfiguracoes($MetodoNome, $Chave){
    //Busca as Configurações
    const { Client } = require('../../node_modules/pg');
    const client =  new Client({host: '31.220.60.220',user: 'postgres',password: 'Favero10@M',database: 'GestorX',port: 5432});
    client.connect();
    return new Promise((resolve, reject) => client.query("SELECT getotr_valor from getotr_config WHERE getotr_funcao = '"+ $MetodoNome +"' AND getotr_chave = '"+ $Chave +"'", (err, res) => {
        if (err) throw err
        resolve(res.rows[0]['getotr_valor']);
        client.end();
    }));
}