/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
/** @Descrição:  Acessa o Relatorio de Chamados filtrando pelos status: Em atendimento, Encerramento Agendado e Novos
                Busca todos os chamados localizando pelo Proprietario (Não considera Responsavel)
                Salva e atualiza os chamados no banco   */
//@Autor: Matheus Favero


//Endereço de Testes https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=AgentTicketSearch;Subaction=Search;OwnerIDs=25;StateIDs=17;StateIDs=19;StateIDs=16
//GravaChamadoBanco("2023033050000248","Apisul","RES: MOTORISTA SUSPENSO OPENTECH","Encerrado","Atendimento","Gabriel Spagnol","Gabriel Spagnol","30/03/2023 15:00","30/03/2023 15:00");


/** Importação dos Modulos **/ 
const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');


/** Importa o Navegador Padrão para o Método **/ 
browser = require('../BuscaChamadosPorCliente/Navegador');




//**  IMPLEMENTAÇÃO  **//



// Função que inicia a Execução do Processo e devolve os dados para a Requisição WS 
// @param $CodigoUsuario Codigo do Usuario enviado pela Requisição WS
// @Return Retorna o Status 'Chamados Atualizados' caso o processo foi realizado com sucesso

async function BuscaChamadosSobGestao($CodigoUsuario) {

    //Importa o Pool de Conexão com o Banco de Dados
    const $db_config = require('../../db_config');
    let $PoolConexao = $db_config.Conexao();

    EscreveConsole("Iniciando Processo de Atualização dos Chamados sob Gestão do usuario Cód. " + $CodigoUsuario, 'Log');
    let _ChamadosCarregados = await FuncBuscaChamados($CodigoUsuario, $PoolConexao);
    if(_ChamadosCarregados == true){
        //Se a função for concluida, responde a requisição
        return 'Chamados Atualizados';
    }
}


// Função que Acessa e Executa o Scraping na Pagina do OTRS
// @Param $CodigoUsuario Codigo do Usuario enviado pela Requisição WS
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @return Retorna "True" caso o processo foi concluido com sucesso
async function FuncBuscaChamados($CodigoUsuario, $PoolConexao) {

    async function BuscaCodOTRSUsuario($CodigoUsuario, $PoolConexao){
        //Função para localizar o Codigo do OTRS do Usuario
        return new Promise((resolve, reject) => $PoolConexao.query("SELECT * FROM user_login WHERE id = '"+ $CodigoUsuario +"'", (err, res) => {
            if (err) throw err
            resolve(res.rows[0]['user_otrs_codigo']);
        }));
    }

    $CodigoUsuario = await BuscaCodOTRSUsuario($CodigoUsuario, $PoolConexao);   //Busca o Código do OTRS do Usuario Recebido
    let _OTRSChamadosGestao = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=AgentTicketSearch;Subaction=Search;OwnerIDs=' + $CodigoUsuario + ';StateIDs=17;StateIDs=19;StateIDs=16';

    const page2 = await browser.newPage();
    await page2.goto(_OTRSChamadosGestao, { waitUntil: 'networkidle0'});    //Navega para a Pagina
    const content = await page2.content();                                  //Pega o HTML da Pagina
    const $html = cheerio.load(content);                                    //Carrega o HTML
    page2.close();                                                          //Fecha a Pagina do Navegador após a conclusão do content

    let _LinhasQuantidade = $html('tr').length;
    let _FilaDeChamados = 0;

    //Busca todos os chamados da pagina
    for(let i = 0; i < _LinhasQuantidade; i++){
        const _Ticket =       $html('tr:eq('+ i +')').find('td:eq(3)').text().trim();                                                                                       //Numero do Chamado
        const _ClienteNome = $html('tr:eq(' + i + ')').find('td:eq(4)').text().trim().slice(8);                                                                             //Nome do Cliente (ID Do Cliente)
        const _Titulo =       $html('tr:eq('+ i +')').find('td:eq(5)').find('div').attr('title')?.replace("'", "");           //Titulo do Chamado
        const _Estado =       $html('tr:eq('+ i +')').find('td:eq(6)').find('div').attr('title');                                                    //Estado do Chamado
        const _Fila =         $html('tr:eq('+ i +')').find('td:eq(7)').find('div').attr('title');                                                    //Fila do Chamado
        const _Responsavel =  $html('tr:eq('+ i +')').find('td:eq(8)').find('div').attr('title')?.split('(')[0].trim();                     //Responsavel
        const _Proprietario = $html('tr:eq('+ i +')').find('td:eq(9)').find('div').attr('title')?.split('(')[0].trim();                     //Proprietario
        const _CriadoEm =     $html('tr:eq('+ i +')').find('td:eq(10)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Cadastro do Chamado
        const _AlteradoEm =   $html('tr:eq('+ i +')').find('td:eq(11)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Alteração do Chamado
        _FilaDeChamados++

        //console.log(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm);
        await GravaChamadoBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm,$PoolConexao);
    }
    if(_FilaDeChamados >= _LinhasQuantidade){
        EscreveConsole("Processo integrou " + _LinhasQuantidade + " chamados com Sucesso!", 'Log');
        return true;
    }
}





// Função que realiza a Inserção/Atualização/Edição dos dados do Chamado encontrado no Banco de dados
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @params Dados dos chamados encontrados
async function GravaChamadoBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm, $PoolConexao){

    //Busca o Código do Cliente
    let _SQL_BuscaClienteCod = "SELECT cli_codigo FROM cli_clientes WHERE cli_nome_otrs ILIKE '%" + _ClienteNome +"%';";
    let _ClienteID = await new Promise((resolve, reject) => $PoolConexao.query(_SQL_BuscaClienteCod, (err, res) => {
        if(err){
            console.log(err.message);
            console.log(_SQL_BuscaClienteCod);
        }

        if(res.rowCount == 1){
            resolve(res.rows[0]['cli_codigo']);
        }
        else if(res.rowCount > 1){
            resolve(0);
        }
        else{
            resolve(null);
        }
        return resolve;
    }));

    let _SQL_BuscaChamado    = "SELECT ccha_codigo FROM ccha_cliente_chamado WHERE ccha_ticket = '" + _Ticket + "'";
    let _SQL_InsereChamado   = "INSERT INTO ccha_cliente_chamado(ccha_ticket, ccha_titulo, ccha_estado, ccha_fila, ccha_data_cadastro, ccha_data_alteracao, ccha_responsavel, ccha_proprietario, ccha_cli_codigo, ccha_data_integracao) VALUES ('"+ _Ticket + "', '"+_Titulo+"', '"+_Estado+"', '"+_Fila+"', to_timestamp('"+_CriadoEm+"', 'DD/MM/YYYY HH24'), to_timestamp('"+_AlteradoEm+"', 'DD/MM/YYYY HH24'),'"+_Responsavel+"','"+_Proprietario+"', '"+_ClienteID+"',now());";
    let _SQL_AtualizaChamado = "UPDATE public.ccha_cliente_chamado SET ccha_titulo='"+_Titulo+"', ccha_estado='"+_Estado+"', ccha_fila='"+_Fila+"', ccha_data_cadastro=to_timestamp('"+_CriadoEm+"','DD/MM/YYYY HH24'), ccha_data_alteracao=to_timestamp('"+_AlteradoEm+"','DD/MM/YYYY HH24'), ccha_responsavel='"+_Responsavel+"', ccha_proprietario='"+_Proprietario+"' WHERE ccha_ticket = '"+_Ticket+"'";

    EscreveConsole("Criando/Atualizando Chamado: Ticket: " +_Ticket+ " NomeCliente: " +_ClienteNome+ " CodigoEncontrado: " + _ClienteID + "", 'Log');

    if(_ClienteID != null) {

        $PoolConexao.query(_SQL_BuscaChamado, (err, res) => {
            if(err){
                EscreveConsole(err.message, 'Erro');
                EscreveConsole("SQL utilizado no Processo: " + _SQL_BuscaChamado, 'Erro');
            }
            else if(res.rowCount == 0){
                //console.log("Chamado não Encontrado, Gravando...");
                
                //Salva os Dados do Chamado
                $PoolConexao.query(_SQL_InsereChamado, (err, res) => {
                    if(err){
                        EscreveConsole(err.message, 'Erro');
                        EscreveConsole("SQL utilizado no Processo: " + _SQL_InsereChamado, 'Erro');
                    }
                    //console.log("Chamado Inserido com Sucesso!");
                });
            }
            else{
                //console.log("Chamado Duplicado, Atualizando...");
    
                //Atualiza o Chamado já Existente
                $PoolConexao.query(_SQL_AtualizaChamado, (err, res) => {
                    if(err){
                        EscreveConsole(err.message, 'Erro');
                        EscreveConsole("SQL utilizado no Processo: " + _SQL_AtualizaChamado, 'Erro');
                    }
                    //console.log("Chamado Atualizado com Sucesso!");
                });
            }
        });
    }
    else {
        EscreveConsole("Chamado: " + _Ticket + " | Cliente: '" + _ClienteNome + "' não foi encontrado no Banco de dados, verifique o cadastro de clientes!", 'Log');
    }
    return
}


// Função que busca os dados de Configurações na tabela 'getotr_config'
// @param $MetodoNome Nome do Método que está chamando a Configuração
// @param $Chave Nome da Chave que contem o valor da Configuração
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna o valor da Chave

async function BuscaConfiguracoes($MetodoNome, $Chave, $PoolConexao){
    //Busca as Configurações
    return new Promise((resolve, reject) => $PoolConexao.query("SELECT getotr_valor from getotr_config WHERE getotr_funcao = '"+ $MetodoNome +"' AND getotr_chave = '"+ $Chave +"'", (err, res) => {
        if (err) throw err
        resolve(res.rows[0]['getotr_valor']);
    }));
}

// Função que escreve os logs no Console da VM
// @param $Texto Texto a ser exibido
// @param $Tipo Tipo da mensagem (Log, Erro)

function EscreveConsole($Texto, $Tipo){
    if($Tipo == 'Erro'){
        console.log("[Erro > BuscaChamadosSobGestão] - " + new Date().toLocaleTimeString() + " - " + $Texto);
    } else {
        console.log("[BuscaChamadosSobGestão] - " + new Date().toLocaleTimeString() + " - " + $Texto);
    }
}

/** Modulo de Exportação dos Dados */
module.exports = {
    BuscaChamadosSobGestao: BuscaChamadosSobGestao,
};