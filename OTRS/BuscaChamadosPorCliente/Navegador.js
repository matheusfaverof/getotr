/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
// @Descrição:  Cria e Abre um Navegador ao iniciar o WebService, Após realiza Login no OTRS usando as credenciais de acesso)
// @Autor: Matheus Favero


/** Importação dos Modulos **/ 
const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');


/// URL da pagina de Login do OTRS
var _OTRSLoginURL = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl';

/// Realiza a Chamada Inicial da Função
Main();



//**  IMPLEMENTAÇÃO  **//


// Função que executa e carrega as configurações iniciais do Navegador.
// Após concluir o Login, o a execução do Script ira parar, porem o navegador continuará me execução

async function Main(){

    //Importa o Pool de Conexão com o Banco de Dados
    const $db_config = require('../../db_config');
    $PoolConexao = $db_config.Conexao();


    // == Configurações == //
    browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        headless: true,
        timeout: 15000000,
    });        

    page = await browser.newPage();                             //Cria uma nova Pagina
    await page.setViewport({width: 1200, height: 720});         //Seta o ViewPort da Pagina
    await Login($PoolConexao);                                          //Realiza Login no OTRS
    EscreveConsole("Navegador Aberto com Sucesso!");            //Log
}


// Função que realiza o Login no Navegador.
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna "True" caso o login seja concluído com sucesso.

async function Login($PoolConexao){
    EscreveConsole("Iniciando processo de Login no OTRS...");
    await page.goto(_OTRSLoginURL, { waitUntil: 'networkidle0' });                                      // Navega para a Pagina de Login
    await page.type('#User', await BuscaConfiguracoes("LoginOTRS", "Usuario", $PoolConexao));           // Usuario
    await page.type('#Password', await BuscaConfiguracoes("LoginOTRS", "Senha", $PoolConexao));         // Senha

    await Promise.all([
        page.click('#LoginButton'),                             //Clica no Botão de Entrar
        page.waitForNavigation({ waitUntil: 'networkidle0' }),  //Aguarda o Carregamento da Pagina 
    ]);
    EscreveConsole("Login realizado com Sucesso!");
    return true;
}

// Função que Retorna as configurações solicitadas do Banco de Dados
// @Param $MetodoNome Nome do Método requisitado
// @Param $Chave Nome da Chave que contem a informação
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna o Valor contido na Chave

async function BuscaConfiguracoes($MetodoNome, $Chave, $PoolConexao){
    $PoolConexao.connect();
    return new Promise((resolve, reject) => $PoolConexao.query("SELECT * FROM gcon_getotr_configuracoes WHERE gcon_funcao = '"+ $MetodoNome +"' AND gcon_chave = '"+ $Chave +"'", (err, res) => {
        if (err) throw err
        resolve(res.rows[0]['gcon_valor']);
    }));
}

// Função que escreve os logs no Console da VM
// @param $Texto Texto a ser exibido

function EscreveConsole($Texto){
    console.log("[Navegador] - " + new Date().toLocaleTimeString() + " - " + $Texto);
}