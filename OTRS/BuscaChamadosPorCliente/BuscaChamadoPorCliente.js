/***************************************************************/
/**                                                           **/
/**                     GestorX | GetOTR                      **/
/**     Copyright (c) 2023 - Todos os Direitos Reservados     **/
/**                                                           **/
/**                                                           **/
/***************************************************************/
// @Descrição:  Busca todos os Chamados Abertos de um Cliente no OTRS
// @Autor: Matheus Favero

// Util: https://gist.github.com/tokland/d3bae3b6d3c1576d8700405829bbdb52
// Usuarios para Teste
    // joao.vitor
    // 12345678






/** Importação dos Modulos **/ 
const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');


/** Importa o Navegador Padrão para o Método **/ 
browser = require('./Navegador');  //Importa o Navegador padrão para o método




// Função que inicia a Execução do Processo e devolve os dados para a Requisição WS
// @Param $Codigo Codigo do Cliente cadastrado no 'GestorX' (Recebido via WS)
// @Return Retorna o Status 'Cliente Atualizado' caso a execução ocorre com Sucesso.

async function BuscaChamadoAbertoCliente($Codigo) {

    //Importa o Pool de Conexão com o Banco de Dados
    const $db_config = require('../../db_config');
    $PoolConexao = $db_config.Conexao();

    EscreveConsole("Iniciando Processo de Atualização do Cliente Cód: " + $Codigo, 'Log');
    _ChamadosCarregados = await BuscaChamadosAbertos($Codigo, $PoolConexao);
    if(_ChamadosCarregados == true){
        return 'Cliente Atualizado';
    }
}


// Função que Acessa e Executa o Scraping na Pagina do OTRS
// @Param $Codigo Codigo do Cliente cadastrado no 'GestorX' (Recebido via WS)
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Return Retorna 'True' caso a execução ocorra com sucesso

async function BuscaChamadosAbertos($Codigo, $PoolConexao){


    const _ChamadosNumeroTicket = []; //Lista de Chamados Encontrados

    // Função que Busca o Codigo do Cliente do OTRS utilizando o Codigo do GestorX (De/Para)
    // @Param $Codigo Codigo do Cliente no GestorX 
    // @Param $PoolConexao Pool de Conexão com o Banco de Dados
    // @Return Retorna o Codigo do Cliente do OTRS
 
    async function BuscaIDCliente($Codigo, $PoolConexao){
        var SQL = "SELECT cli_codigo FROM cli_clientes cc where cli_codigo = " + $Codigo;
        return new Promise((resolve, reject) => $PoolConexao.query(SQL, (err, res) => {
            if(err){
                console.log("[Atualizador] - " + new Date().toLocaleTimeString() + " - Erro ao Buscar Lista de Clientes:" + err.message);
            }else{
                var $Customer = res.rows[0]['cli_customer_id'];
                console.log($Customer);
                resolve($Customer);
            }
            console.log("[Atualizador] - " + new Date().toLocaleTimeString() + " - Lista de Cliente Obtida com Sucesso!");
            return resolve;
        }));
    }


    var $ClienteIDCustomer = await BuscaIDCliente($Codigo, $PoolConexao)
    var _OTRSChamadosAbertosURL =   'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?Action=AgentTicketSearch;Subaction=Search;StateType=Open;CustomerIDRaw=' + $ClienteIDCustomer;
    const page2 = await browser.newPage();
    await page2.goto(_OTRSChamadosAbertosURL, { waitUntil: 'networkidle0' });        //Navega para a Pagina do Cliente
    const content = await page2.content();                                           //Pega o HTML da Pagina
    const $html = cheerio.load(content);                                             //Carrega o HTML
    page2.close();                                                                   //Fecha a pagina do navegador após a conclusão do content

    var _LinhasQuantidade = $html('tr').length;
    var _FilaDeChamados = 0;                                                         //Verifica a Quantidade de Registros com a TAG <TR>

    /*
        ORDEM DO DE PARA

        1. Numero do Chamado
        2. Idade
        3. Remetente
        4. Titulo
        5. Estado
        6. Responsável
        7. Bloquear
        8. Fila
        9. Proprietario
        10. ID do Cliente
        11. Criado
        12. Alterado
    */


    //Busca todos os chamados do cliente no OTRS
    for(var i = 0; i < _LinhasQuantidade; i++){
        /*var _Ticket =       $html('tr:eq('+ i +')').find('td:eq(3)').text().trim();                                                                     //Numero do Chamado
        var _Idade =        $html('tr:eq('+ i +')').find('td:eq(4)').find('div').attr('title');                                                         //Idade do Chamado  
        var _Remetente =    $html('tr:eq('+ i +')').find('td:eq(5)').find('div').attr('title');                                                         //Remetente
        var _Titulo =       $html('tr:eq('+ i +')').find('td:eq(6)').find('div').attr('title')?.replace("'", "");                                       //Titulo do Chamado
        var _Estado =       $html('tr:eq('+ i +')').find('td:eq(7)').find('div').attr('title');                                                         //Estado do Chamado
        var _Responsavel  = $html('tr:eq('+ i +')').find('td:eq(8)').find('div').attr('title');                                                         //Responsavel
        var _Fila =         $html('tr:eq('+ i +')').find('td:eq(10)').find('div').attr('title');                                                        //Fila do Chamado
        var _CriadoEm =     $html('tr:eq('+ i +')').find('td:eq(13)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Cadastro do Chamado
        var _AlteradoEm =   $html('tr:eq('+ i +')').find('td:eq(14)').find('div').attr('title')?.replace(' (America/Sao_Paulo)',''); */                   //Alteração do Chamado
        

        var _Ticket =       $html('tr:eq('+ i +')').find('td:eq(3)').text().trim();                                                                     //Numero do Chamado
        var _ClienteNome =  $html('tr:eq('+ i +')').find('td:eq(4)').text().trim().slice(8);                                                            //Nome do Cliente  
        var _Titulo =       $html('tr:eq('+ i +')').find('td:eq(5)').find('div').attr('title')?.replace("'", "");                                       //Titulo do Chamado
        var _Estado =       $html('tr:eq('+ i +')').find('td:eq(6)').find('div').attr('title');                                                         //Estado do Chamado
        var _Fila =         $html('tr:eq('+ i +')').find('td:eq(7)').find('div').attr('title');                                                         //Fila do Chamado
        var _Responsavel =  $html('tr:eq('+ i +')').find('td:eq(8)').find('div').attr('title');                                                         //Responsavel
        var _Proprietario = $html('tr:eq('+ i +')').find('td:eq(9)').find('div').attr('title');                                                         //Proprietario
        var _CriadoEm =     $html('tr:eq('+ i +')').find('td:eq(10)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');                    //Cadastro do Chamado
        var _AlteradoEm =   $html('tr:eq('+ i +')').find('td:eq(11)').find('div').attr('title')?.replace(' (America/Sao_Paulo)','');

        _ChamadosNumeroTicket.unshift("'"+_Ticket+"'"); //Adiciona o Numero do Chamado na Lista de Chamados Abertos
        _FilaDeChamados = _FilaDeChamados + await GravaChamadoBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm,$Codigo, $PoolConexao);
    }
    await AlteraStatusChamado(_ChamadosNumeroTicket, $Codigo, $PoolConexao);    //Chama a função para verificar e encerrar os chamados não listados no array
    EscreveConsole("Foi integrado " + _LinhasQuantidade + " Chamados com Sucesso!", 'Log');
    if(_FilaDeChamados >= _LinhasQuantidade){
        return true;
    }
}



// Função que realiza a Inserção/Atualização/Edição dos dados do Chamado encontrado no Banco de dados
// @Param _ClienteID Codigo do Cliente cadastrado no 'GestorX' (Recebido via WS)
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @params Dados dos chamados encontrados

async function GravaChamadoBanco(_Ticket,_ClienteNome,_Titulo,_Estado,_Fila,_Responsavel,_Proprietario,_CriadoEm,_AlteradoEm,_ClienteID, $PoolConexao){

    //SQL's

    //var SQL_BuscaChamado = "SELECT * FROM cchao_clientes_chamados_otrs ccco WHERE cchao_ticket = '" + _Ticket +"'";
    //var SQL_InsereChamado = "INSERT INTO public.cchao_clientes_chamados_otrs(cchao_ticket, cchao_idade, cchao_titulo, cchao_estado, cchao_fila, cchao_criado, cchao_alterado, cchao_cliente_codigo) VALUES('"+ _Ticket + "', '"+_Idade+"', '"+_Titulo+"', '"+_Estado+"', '"+_Fila+"', '"+_CriadoEm+"', '"+_AlteradoEm+"', '"+_ClienteID+"');";
    //var SQL_AtualizaChamado = "UPDATE public.cchao_clientes_chamados_otrs SET cchao_idade='"+_Idade+"', cchao_titulo='"+_Titulo+"', cchao_estado='"+_Estado+"', cchao_fila='"+_Fila+"', cchao_criado='"+_CriadoEm+"', cchao_alterado='"+_AlteradoEm+"' WHERE cchao_ticket = '"+_Ticket+"'";

    var _SQL_BuscaChamado       = "SELECT * FROM ccha_cliente_chamado ccc WHERE ccha_chamado_ticket = '" + _Ticket + "'";
    var _SQL_InsereChamado      = "INSERT INTO public.ccha_cliente_chamado(ccha_chamado_ticket, ccha_chamado_titulo, ccha_otrs_estado, ccha_otrs_fila, ccha_otrs_data_cadastro, ccha_otrs_data_alteracao, ccha_otrs_responsavel, ccha_otrs_proprietario, ccha_cli_codigo) VALUES('"+ _Ticket + "', '"+_Titulo+"', '"+_Estado+"', '"+_Fila+"', '"+_CriadoEm+"', '"+_AlteradoEm+"','"+_Responsavel+"','"+_Proprietario+"', '"+_ClienteID+"');";
    var _SQL_AtualizaChamado    = "UPDATE public.ccha_cliente_chamado SET ccha_chamado_titulo='"+_Titulo+"', ccha_otrs_estado='"+_Estado+"', ccha_otrs_fila='"+_Fila+"', ccha_otrs_data_cadastro='"+_CriadoEm+"', ccha_otrs_data_alteracao='"+_AlteradoEm+"', ccha_otrs_responsavel='"+_Responsavel+"', ccha_otrs_proprietario='"+_Proprietario+"' WHERE ccha_chamado_ticket = '"+_Ticket+"'";

    //_Ticket,_Idade,_Remetente,_Titulo,_Estado,_Responsavel,_Fila,_CriadoEm,_AlteradoEm,_ClienteID
    //Verifica se já existe um Chamado Salvo com o Numero do Chamado recebido
    $PoolConexao.query(_SQL_BuscaChamado, (err, res) => {
        if(err){
            EscreveConsole(err.message, 'Erro');
            EscreveConsole("SQL Utilizado no Processo: " + _SQL_BuscaChamado, 'Erro');
        }
        else if(res.rowCount == 0){
        
            //Salva os Dados do Chamado
            $PoolConexao.query(_SQL_InsereChamado, (err, res) => {
                if(err){
                    EscreveConsole(err.message, 'Erro');
                    EscreveConsole("SQL Utilizado no Processo: " + _SQL_InsereChamado, 'Erro');
                }
                //console.log("Chamado Inserido com Sucesso!");
            });
        }
        else{
            //Atualiza o Chamado já Existente
            $PoolConexao.query(_SQL_AtualizaChamado, (err, res) => {
                if(err){
                    EscreveConsole(err.message, 'Erro');
                    EscreveConsole("SQL Utilizado no Processo: " + _SQL_AtualizaChamado, 'Erro');
                }
                //console.log("Chamado Atualizado com Sucesso!");
            });
        }
    });
    return true; 
}



// Função que realiza a alteração do Status dos Chamados no GestorX de acordo com o OTRS
// @Param $Chamados Array contendo o numero de todos os chamados encontrados na Pagina do OTRS 
// @Param $PoolConexao Pool de Conexão com o Banco de Dados
// @Param $ClienteID Codigo do Cliente cadastrado no 'GestorX'

async function AlteraStatusChamado($Chamados, $ClienteID, $PoolConexao){

    //Encerra todos os chamados do Cliente que não estão abertos
    //Verifica os encerrados, olhando para a lista da chamada,
    //Caso o chamado não estiver na lista, o mesmo se encontra fechado

    var SQL_AlteraEstadoChamado = "UPDATE ccha_cliente_chamado SET ccha_otrs_estado = 'Encerrado' WHERE ccha_chamado_ticket NOT IN ("+$Chamados+") AND ccha_cli_codigo = "+$ClienteID+";";
    
    if($Chamados.length > 0){
        $PoolConexao.query(SQL_AlteraEstadoChamado, (err, res) => {
            if(err){
                EscreveConsole("Ocorreu um Erro ao alterar o status dos chamados Fechados!", 'Erro');
                EscreveConsole("Mensagem de ERRO: " + err.message, 'Erro');
                EscreveConsole("SQL da Consulta: " + SQL_AlteraEstadoChamado, 'Erro');
            }
        });
        EscreveConsole("Chamados com Status Encerrado no OTRS foram fechados com Sucesso!", 'Log');
    }
    else{
        EscreveConsole("Nenhum chamado com Status de Encerrado para finalizar no GestorX", 'Log');
    }
}


// Função que escreve os logs no Console da VM
// @param $Texto Texto a ser exibido
// @param $Tipo Tipo da mensagem (Log, Erro)

function EscreveConsole($Texto, $Tipo){
    if($Tipo == 'Erro'){
        console.log("[Erro > BuscaChamadosPorCliente] - " + new Date().toLocaleTimeString() + " - " + $Texto);
    } else {
        console.log("[BuscaChamadosPorCliente] - " + new Date().toLocaleTimeString() + " - " + $Texto);
    }
}


/** Modulo de Exportação dos Dados */
module.exports = {
    BuscaChamadoAbertoCliente: BuscaChamadoAbertoCliente,
};
